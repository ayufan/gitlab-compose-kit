COMMA := ,

.PHONY: create-dev
create-dev: RAILS_ENV=development
create-dev: deps
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh /scripts/helpers/create-dev-env.sh

.PHONY: create-test
create-test: RAILS_ENV=test
create-test: deps
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh bin/rake -t db:drop db:prepare

.PHONY: create-runner
create-runner: RAILS_ENV=development
create-runner: deps
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh bin/rails runner "Ci::Runner.create(runner_type: :instance_type, token: 'SHARED_RUNNER_TOKEN')"

.PHONY: create
create: create-dev create-test create-runner

.PHONY: migrate
migrate: migrate-dev migrate-test

.PHONY: migrate-dev
migrate-dev: RAILS_ENV=development
migrate-dev:
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh bin/rake db:migrate

.PHONY: migrate-dev-down
migrate-dev-down: RAILS_ENV=development
migrate-dev-down: migrate-down

.PHONY: migrate-test
migrate-test: RAILS_ENV=test
migrate-test:
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh bin/rake db:migrate

.PHONY: migrate-test-down
migrate-test-down: RAILS_ENV=test
migrate-test-down: migrate-down

.PHONY: migrate-down
migrate-down: DB?=
migrate-down:
ifeq (,$(VERSION))
	$(error "Specify VERSION=")
endif
	@echo "Migrating down $(DB) to version $(VERSION) in $(RAILS_ENV)"
	-$(DOCKER_COMPOSE) run spring /scripts/entrypoint/gitlab-rails-exec.sh bin/rake \
			$(if $(DB),$(addprefix db:migrate:down:, $(subst $(COMMA), ,$(DB))),db:migrate:down) \
			VERSION=$(VERSION)

.PHONY: update-dev
update-dev: update-repos
	make migrate-dev

.PHONY: update-test
update-test: update-repos
	make migrate-test

.PHONY: update
update: update-dev update-test

.PHONY: assets-compile
assets-compile: RAILS_ENV=test
assets-compile:
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh bin/rake gitlab:assets:compile

.PHONY: webpack-compile
webpack-compile: deps
	$(DOCKER_COMPOSE) run -e FORCE_WEBPACK_COMPILE=true webpack

.PHONY: gitaly-compile
gitaly-compile: deps
	$(DOCKER_COMPOSE) run -e FORCE_GITALY_COMPILE=true gitaly

.PHONY: rails-compile
rails-compile: RAILS_ENV=development
rails-compile: deps
	$(DOCKER_COMPOSE) run \
		spring /scripts/entrypoint/gitlab-rails-exec.sh /bin/true

.PHONY: env
env:
	./scripts/env bash

.PHONY: ports
ports:
	./scripts/env ./scripts/ports

.PHONY: volumes-usage
volumes-usage:
	./scripts/env ./scripts/volumes-usage

.PHONY: recover-postgres-replica
recover-postgres-replica:
	# reset WAL
	$(DOCKER_COMPOSE_AUX) stop postgres
	$(DOCKER_COMPOSE_AUX) run --rm -u postgres postgres bash -c 'pg_resetwal -f "$$PGDATA"'
	# recreate replica
	$(DOCKER_COMPOSE_AUX) rm -v -f -s postgres-replica
	$(DOCKER_COMPOSE_AUX) up -d postgres-replica
